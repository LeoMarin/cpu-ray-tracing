#pragma once

#include "transform.h"
#include "material.h"

class Shape
{
public:
	// Shape Intefrace
	Shape() {}
	Shape(Transform ObjectToWorld) : ObjectToWorld(ObjectToWorld), WorldToObject(Transform(ObjectToWorld.GetInverseMatrix())) {}
	Shape(Transform ObjectToWorld, Transform WorldToObject) : ObjectToWorld(ObjectToWorld), WorldToObject(WorldToObject) {}
	virtual ~Shape() {}
	// Return true if ray intersects shape
	virtual bool Intersect(const Ray& ray, SurfaceInteraction *isect) const = 0;
	// Returns AABB of shape in object space
	virtual Bounds3 ObjectBound() const = 0;
	// Returns AABB of shape in world space
	virtual Bounds3 WorldBound() const { return ObjectToWorld(ObjectBound()); }

	// Returns point inside shape
	virtual Point3f PointInsideShape() const { return Point3f(); }

	// Material
	virtual Material* ShapeMaterial() const { return nullptr; }
protected:
	// Shape public data
	const Transform ObjectToWorld, WorldToObject;
};