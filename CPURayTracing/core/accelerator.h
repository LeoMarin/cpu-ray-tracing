#pragma once

#include "geometry.h"
#include "interaction.h"

// Primitive Declarations
class Accelerator
{
public:
    // Primitive Interface
    virtual ~Accelerator() {}
    virtual bool Intersect(const Ray& ray, SurfaceInteraction* isect) const = 0;
    //virtual bool IntersectP(const Ray &r) const = 0;
};
