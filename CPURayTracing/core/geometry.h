#pragma once

#include <cmath>
#include <limits>
#include <algorithm>
#include <iostream>

#define MachineEpsilon (std::numeric_limits<float>::epsilon() * 0.5f)

class Vector3f;
class Point3f;
class Normal3f;

static constexpr float ShadowEpsilon = 0.0001f;
static constexpr float Pi =         3.14159265f;
static constexpr float InvPi =      0.31830988f;
static constexpr float Inv2Pi =     0.15915494f;
static constexpr float Inv4Pi =     0.07957747f;
static constexpr float PiOver2 =    1.57079632f;
static constexpr float PiOver4 =    0.78539816f;
static constexpr float Sqrt2 =      1.41421356f;
static constexpr float Infinity =   std::numeric_limits<float>::infinity();

inline float gamma(int n) { return (n * MachineEpsilon) / (1 - n * MachineEpsilon); }
inline float Radians(float deg) { return (Pi / 180) * deg; }
inline float Degrees(float rad) { return 180 * InvPi * rad; }

// find real roots of quadratic equation
// return false if no real solutions
inline bool Quadratic(float a, float b, float c, float* t0, float* t1)
{
    float discriminatn = b * b - 4 * a * c;
    
    // no real solutions
    if (discriminatn < 0.f || a == 0)
        return false;

    float rootDiscriminant = sqrt(discriminatn);
    float aInv = 0.5f / a;

    *t0 = (-b - rootDiscriminant) * aInv;
    *t1 = (-b + rootDiscriminant) * aInv;
    
    // t0 should always be smaller
    if (*t0 > *t1) std::swap(*t0, *t1);
    return true;
}

/*
/
/ *** Vector3f ***
/
*/
class Vector3f
{
public:
    // Vector3f public methods
    Vector3f() : x(0), y(0), z(0) {}
    Vector3f(float f) : x(f), y(f), z(f) {}
    Vector3f(float x, float y, float z) : x(x), y(y), z(z) {}
    Vector3f(const Vector3f& other) : x(other.x), y(other.y), z(other.z) {}

    explicit Vector3f(const Point3f& p);
    explicit Vector3f(const Normal3f& n);
    ~Vector3f() {}

    inline float Length() const { return sqrt(x * x + y * y + z * z); }
    inline float LengthSquared() const { return x * x + y * y + z * z; }

    inline const Vector3f& operator+() const { return *this; }
    inline Vector3f operator-() const { return Vector3f(-x, -y, -z); }
    inline float operator[](int i) const { return (i == 0) ? x : ((i == 1) ? y : z); }
    inline float& operator[](int i) { return (i == 0) ? x : ((i == 1) ? y : z); };

    inline Vector3f& operator+=(const Vector3f &v2);
    inline Vector3f& operator-=(const Vector3f &v2);
    inline Vector3f& operator*=(const Vector3f &v2);
    inline Vector3f& operator/=(const Vector3f &v2);
    inline Vector3f& operator*=(const float t);
    inline Vector3f& operator/=(const float t);

    bool operator==(const Vector3f& n) const { return x == n.x && y == n.y && z == n.z; }
    bool operator!=(const Vector3f &n) const { return x != n.x || y != n.y || z != n.z; }

public:
    // Vector3f public data
    float x, y, z;
};

// iostream operator overloading
inline std::istream& operator>>(std::istream &is, Vector3f &t) {
    is >> t.x >> t.y >> t.z;
    return is;
}

inline std::ostream& operator<<(std::ostream &os, const Vector3f &t) {
    os << t.x << " " << t.y << " " << t.z;
    return os;
}

// implementation of Vector3f public methods

inline Vector3f operator+(const Vector3f &v1, const Vector3f &v2) {
    return Vector3f(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

inline Vector3f operator-(const Vector3f &v1, const Vector3f &v2) {
    return Vector3f(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

inline Vector3f operator*(const Vector3f &v1, const Vector3f &v2) {
    return Vector3f(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
}

inline Vector3f operator/(const Vector3f &v1, const Vector3f &v2) {
    return Vector3f(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
}

inline Vector3f operator*(float t, const Vector3f &v) {
    return Vector3f(t*v.x, t*v.y, t*v.z);
}

inline Vector3f operator/(Vector3f v, float t) {
    float inv = 1 / t;
    return Vector3f(v.x * inv, v.y * inv, v.z * inv);
}

inline Vector3f operator*(const Vector3f &v, float t) {
    return Vector3f(t*v.x, t*v.y, t*v.z);
}

// operator overloading
inline Vector3f& Vector3f::operator+=(const Vector3f &v){
    x  += v.x;
    y  += v.y;
    z  += v.z;
    return *this;
}

inline Vector3f& Vector3f::operator*=(const Vector3f &v){
    x  *= v.x;
    y  *= v.y;
    z  *= v.z;
    return *this;
}

inline Vector3f& Vector3f::operator/=(const Vector3f &v){
    x  /= v.x;
    y  /= v.y;
    z  /= v.z;
    return *this;
}

inline Vector3f& Vector3f::operator-=(const Vector3f& v) {
    x  -= v.x;
    y  -= v.y;
    z  -= v.z;
    return *this;
}

inline Vector3f& Vector3f::operator*=(const float t) {
    x  *= t;
    y  *= t;
    z  *= t;
    return *this;
}

inline Vector3f& Vector3f::operator/=(const float t) {
    float k = 1.0f / t;

    x  *= k;
    y  *= k;
    z  *= k;
    return *this;
}

inline Vector3f UnitVector(Vector3f v) {
    return v / v.Length();
}

/*
/
/ *** Point3f ***
/
*/

class Point3f
{

public:
    // Point3f Public Methods
    Point3f() { x = y = z = 0; }
    Point3f(float x, float y, float z) : x(x), y(y), z(z) {}
    explicit Point3f(const Vector3f& v) : x(v.x), y(v.y), z(v.z) {}

    explicit operator Vector3f() const { return Vector3f(x, y, z); } // ??

    Point3f operator+(const Point3f& p) const { return Point3f(x + p.x, y + p.y, z + p.z); }
    Point3f operator+(const Vector3f& v) const { return Point3f(x + v.x, y + v.y, z + v.z); }
    Vector3f operator-(const Point3f& p) const { return Vector3f(x - p.x, y - p.y, z - p.z); }
    Point3f operator-(const Vector3f& v) const { return Point3f(x - v.x, y - v.y, z - v.z); }
    Point3f operator*(float f) const { return Point3f(f * x, f * y, f * z); }
    Point3f operator/(float f) const { float inv = (float)1 / f; return Point3f(inv * x, inv * y, inv * z); }

    inline float operator[](int i) const { return (i == 0) ? x : ((i == 1) ? y : z); }
    inline float& operator[](int i) { return (i == 0) ? x : ((i == 1) ? y : z); };
    Point3f operator-() const { return Point3f(-x, -y, -z); }

    Point3f& operator+=(const Vector3f& v);
    Point3f& operator-=(const Vector3f& v);
    Point3f& operator+=(const Point3f& p);
    Point3f& operator*=(float f);
    Point3f& operator/=(float f);

    bool operator==(const Point3f &p) const { return x == p.x && y == p.y && z == p.z; }
    bool operator!=(const Point3f &p) const { return x != p.x || y != p.y || z != p.z; }

    // Point3f Public Data
    float x, y, z;
};

inline Point3f& Point3f::operator+=(const Vector3f& v)
{
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
}

inline Point3f& Point3f::operator-=(const Vector3f& v)
{
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
}

inline Point3f& Point3f::operator+=(const Point3f& p)
{
    x += p.x;
    y += p.y;
    z += p.z;
    return *this;
}

inline Point3f& Point3f::operator*=(float f)
{
    x *= f;
    y *= f;
    z *= f;
    return *this;
}

inline Point3f& Point3f::operator/=(float f)
{
    float inv = (float)1 / f;
    x *= inv;
    y *= inv;
    z *= inv;
    return *this;
}

/*
/
/ *** Normal3f ***
/
*/

class Normal3f
{

public:
    // Normal3 Public Methods
    Normal3f() { x = y = z = 0; }
    Normal3f(float x, float y, float z) : x(x), y(y), z(z) {}
    explicit Normal3f(const Vector3f &v) : x(v.x), y(v.y), z(v.z) {}

    float LengthSquared() const { return x * x + y * y + z * z; }
    float Length() const { return std::sqrt(LengthSquared()); }

    Normal3f operator+(const Normal3f &n) const { return Normal3f(x + n.x, y + n.y, z + n.z); }
    Normal3f operator-(const Normal3f &n) const { return Normal3f(x - n.x, y - n.y, z - n.z); }
    Normal3f operator*(float f) const { return Normal3f(f * x, f * y, f * z); }
    Normal3f operator/(float f) const { float inv = (float)1 / f; return Normal3f(x * inv, y * inv, z * inv); }

    inline float operator[](int i) const { return (i == 0) ? x : ((i == 1) ? y : z); }
    inline float& operator[](int i) { return (i == 0) ? x : ((i == 1) ? y : z); };
    Normal3f operator-() const { return Normal3f(-x, -y, -z); }

    Normal3f& operator+=(const Normal3f& n);
    Normal3f& operator-=(const Normal3f& n);
    Normal3f& operator*=(float f);
    Normal3f& operator/=(float f);

    bool operator==(const Normal3f& n) const { return x == n.x && y == n.y && z == n.z; }
    bool operator!=(const Normal3f &n) const { return x != n.x || y != n.y || z != n.z; }


    // Normal3 Public Data
    float x, y, z;
};

inline Normal3f& Normal3f::operator+=(const Normal3f& n)
{
    x += n.x;
    y += n.y;
    z += n.z;
    return *this;
}

inline Normal3f& Normal3f::operator-=(const Normal3f& n)
{
    x -= n.x;
    y -= n.y;
    z -= n.z;
    return *this;
}

inline Normal3f& Normal3f::operator*=(float f)
{
    x *= f;
    y *= f;
    z *= f;
    return *this;
}

inline Normal3f& Normal3f::operator/=(float f)
{
    float inv = (float)1 / f;
    x *= inv;
    y *= inv;
    z *= inv;
    return *this;
}

/*
/
/ *** Ray ***
/
*/

class Ray
{
public:
    // Ray public methods
    Ray() :origin(Point3f()), direction(Vector3f()), tMax(1e10) {}
    Ray(const Point3f& origin, const Vector3f& direction, float tMax = 1e10) : origin(origin), direction(direction), tMax(tMax) {}

    ~Ray() {}

    // gets point on ray p(t) = origin + t * direction
    inline Point3f operator()(float t) const { return origin + t * direction; }

public:
    // Ray public data
    Point3f origin;
    Vector3f direction;
    mutable float tMax;
};

/*
/
/ *** Inline functions ***
/
*/

inline Vector3f::Vector3f(const Point3f& p)
    : x(p.x), y(p.y), z(p.z) 
{}
inline Vector3f::Vector3f(const Normal3f& p)
    : x(p.x), y(p.y), z(p.z) 
{}

// Abs
inline Vector3f Abs(const Vector3f& v)
{
    return Vector3f(std::abs(v.x), std::abs(v.y), std::abs(v.z));
}

inline Point3f Abs(const Point3f& p)
{
    return Point3f(std::abs(p.x), std::abs(p.y), std::abs(p.z));
}

inline Normal3f Abs(const Normal3f& n)
{
    return Normal3f(std::abs(n.x), std::abs(n.y), std::abs(n.z));
}

// Dot
inline float Dot(const Vector3f& v1, const Vector3f& v2)
{
    return v1.x *v2.x + v1.y *v2.y  + v1.z *v2.z;
}

inline float Dot(const Vector3f& v1, const Normal3f& n2)
{
    return v1.x *n2.x + v1.y *n2.y  + v1.z *n2.z;
}

inline float Dot(const Normal3f& n1, const Vector3f& v2)
{
    return n1.x *v2.x + n1.y *v2.y  + n1.z *v2.z;
}

inline float Dot(const Normal3f& n1, const Normal3f& n2)
{
    return n1.x *n2.x + n1.y *n2.y  + n1.z *n2.z;
}

// Abs Dot
inline float AbsDot(const Vector3f& v1, const Vector3f& v2)
{
    return std::abs(v1.x *v2.x + v1.y *v2.y  + v1.z *v2.z);
}

inline float AbsDot(const Vector3f& v1, const Normal3f& n2)
{
    return std::abs(v1.x *n2.x + v1.y *n2.y  + v1.z *n2.z);
}

inline float AbsDot(const Normal3f& n1, const Vector3f& v2)
{
    return std::abs(n1.x *v2.x + n1.y *v2.y  + n1.z *v2.z);
}

inline float AbsDot(const Normal3f& n1, const Normal3f& n2)
{
    return std::abs(n1.x *n2.x + n1.y *n2.y  + n1.z *n2.z);
}

// Reflect
inline Vector3f Reflect(const Vector3f& vector, const Normal3f& normal)
{
    // calculate reflection ray around given
    return vector - Vector3f(normal * 2 * Dot(vector, normal));
}

// Cross
inline Vector3f Cross(const Vector3f& v1, const Vector3f& v2)
{
    return Vector3f( (v1.y*v2.z - v1.z*v2.y),
        (-(v1.x*v2.z - v1.z*v2.x)),
        (v1.x*v2.y - v1.y*v2.x));
}

inline Vector3f Cross(const Vector3f& v1, const Normal3f& n2)
{
    return Vector3f( (v1.y*n2.z - v1.z*n2.y),
        (-(v1.x*n2.z - v1.z*n2.x)),
        (v1.x*n2.y - v1.y*n2.x));
}

inline Vector3f Cross(const Normal3f& n1, const Vector3f& v2)
{
    return Vector3f( (n1.y*v2.z - n1.z*v2.y),
        (-(n1.x*v2.z - n1.z*v2.x)),
        (n1.x*v2.y - n1.y*v2.x));
}

// Normalize
inline Vector3f Normalize(const Vector3f& v)
{
    return v / v.Length();
}

inline Normal3f Normalize(const Normal3f& n)
{
    return n / n.Length();
}

// Distance
inline float Distance(const Point3f& p1, const Point3f& p2)
{
    return (p1 - p2).Length();
}

inline float DistanceSquared(const Point3f& p1, const Point3f& p2)
{
    return (p1 - p2).LengthSquared();
}

// Min and max
inline float MinComponent(const Vector3f& v)
{
    return std::min(v.x, std::min(v.y, v.z));
}

inline float MaxComponent(const Vector3f& v)
{
    return std::max(v.x, std::max(v.y, v.z));
}

inline int MaxDimension(const Vector3f& v)
{
    return (v.x > v.y) ? ((v.x > v.z) ? 0 : 2) : ((v.y > v.z) ? 1 : 2);
}

inline Vector3f Min(const Vector3f& p1, const Vector3f& p2)
{
    return Vector3f(std::min(p1.x, p2.x), std::min(p1.y, p2.y),
        std::min(p1.z, p2.z));
}

inline Vector3f Max(const Vector3f& p1, const Vector3f& p2)
{
    return Vector3f(std::max(p1.x, p2.x), std::max(p1.y, p2.y),
        std::max(p1.z, p2.z));
}

inline Point3f Min(const Point3f& p1, const Point3f& p2)
{
    return Point3f(std::min(p1.x, p2.x), std::min(p1.y, p2.y),
        std::min(p1.z, p2.z));
}

inline Point3f Max(const Point3f& p1, const Point3f& p2)
{
    return Point3f(std::max(p1.x, p2.x), std::max(p1.y, p2.y),
        std::max(p1.z, p2.z));
}

// Floor and Ceil
inline Point3f Floor(const Point3f& p)
{
    return Point3f(std::floor(p.x), std::floor(p.y), std::floor(p.z));
}

inline Point3f Ceil(const Point3f& p)
{
    return Point3f(std::ceil(p.x), std::ceil(p.y), std::ceil(p.z));
}

// Clamp
inline Vector3f Clamp(const Vector3f& v)
{
    return Vector3f(
        std::clamp(v.x, 0.0f, 1.0f),
        std::clamp(v.y, 0.0f, 1.0f),
        std::clamp(v.z, 0.0f, 1.0f)
    );
}

/*
/
/ *** Bounds3 ***
/
*/

// Bounds3 Declaration
class Bounds3
{
public:
    // Bounds3 Public Methods
    Bounds3();
    explicit Bounds3(const Point3f& p) : pMin(p), pMax(p) {}
    Bounds3(const Point3f& p1, const Point3f& p2);

    const Point3f& operator[](int i) const;
    Point3f& operator[](int i);

    bool operator==(const Bounds3& b) const
    {
        return b.pMin == pMin && b.pMax == pMax;
    }

    bool operator!=(const Bounds3& b) const
    {
        return b.pMin != pMin || b.pMax != pMax;
    }

    Point3f Corner(int corner) const
    {
        return Point3f((*this)[(corner & 1)].x,
            (*this)[(corner & 2) ? 1 : 0].y,
            (*this)[(corner & 4) ? 1 : 0].z);
    }

    // Returns center of the boungding box
    Point3f Center() const;

    Vector3f Diagonal() const { return pMax - pMin; }

    float SurfaceArea() const
    {
        Vector3f d = Diagonal();
        return 2 * (d.x * d.y + d.x * d.z + d.y * d.z);
    }

    float Volume() const
    {
        Vector3f d = Diagonal();
        return d.x * d.y * d.z;
    }

    int MaximumExtent() const
    {
        Vector3f d = Diagonal();
        if(d.x > d.y && d.x > d.z)
            return 0;
        else if(d.y > d.z)
            return 1;
        else
            return 2;
    }

    Vector3f Offset(const Point3f& p) const
    {
        Vector3f o = p - pMin;
        if(pMax.x > pMin.x) o.x /= pMax.x - pMin.x;
        if(pMax.y > pMin.y) o.y /= pMax.y - pMin.y;
        if(pMax.z > pMin.z) o.z /= pMax.z - pMin.z;
        return o;
    }

    bool IntersectP(const Ray& ray, float* hitt0 = nullptr, float* hitt1 = nullptr) const;

public:
    // Bounds3 Public Data
    Point3f pMin, pMax;
};

inline Bounds3::Bounds3()
{
    float constexpr minNum = std::numeric_limits<float>::lowest();
    float constexpr maxNum = std::numeric_limits<float>::max();
    pMin = Point3f(maxNum, maxNum, maxNum);
    pMax = Point3f(minNum, minNum, minNum);
}

inline Bounds3::Bounds3(const Point3f& p1, const Point3f& p2)
    : pMin(std::min(p1.x, p2.x), std::min(p1.y, p2.y),
        std::min(p1.z, p2.z)),
    pMax(std::max(p1.x, p2.x), std::max(p1.y, p2.y),
        std::max(p1.z, p2.z))
{
}

inline const Point3f& Bounds3::operator[](int i) const
{
    return (i == 0) ? pMin : pMax;
}

inline Point3f& Bounds3::operator[](int i)
{
    return (i == 0) ? pMin : pMax;
}

inline Point3f Bounds3::Center() const
{
    return (pMin + pMax) * 0.5;
}

inline Bounds3 Union(const Bounds3& b, const Point3f& p)
{
    Bounds3 ret;
    ret.pMin = Min(b.pMin, p);
    ret.pMax = Max(b.pMax, p);
    return ret;
}

inline Bounds3 Union(const Bounds3& b1, const Bounds3& b2)
{
    Bounds3 ret;
    ret.pMin = Min(b1.pMin, b2.pMin);
    ret.pMax = Max(b1.pMax, b2.pMax);
    return ret;
}

inline Bounds3 Intersect(const Bounds3& b1, const Bounds3& b2)
{
    // Important: assign to pMin/pMax directly and don't run the Bounds2()
    // constructor, since it takes min/max of the points passed to it.  In
    // turn, that breaks returning an invalid bound for the case where we
    // intersect non-overlapping bounds (as we'd like to happen).
    Bounds3 ret;
    ret.pMin = Max(b1.pMin, b2.pMin);
    ret.pMax = Min(b1.pMax, b2.pMax);
    return ret;
}

inline bool Overlaps(const Bounds3& b1, const Bounds3& b2)
{
    bool x = (b1.pMax.x >= b2.pMin.x) && (b1.pMin.x <= b2.pMax.x);
    bool y = (b1.pMax.y >= b2.pMin.y) && (b1.pMin.y <= b2.pMax.y);
    bool z = (b1.pMax.z >= b2.pMin.z) && (b1.pMin.z <= b2.pMax.z);
    return (x && y && z);
}

inline bool Inside(const Point3f& p, const Bounds3& b)
{
    return (p.x >= b.pMin.x && p.x <= b.pMax.x && p.y >= b.pMin.y &&
        p.y <= b.pMax.y && p.z >= b.pMin.z && p.z <= b.pMax.z);
}

inline bool InsideExclusive(const Point3f& p, const Bounds3& b)
{
    return (p.x >= b.pMin.x && p.x < b.pMax.x&& p.y >= b.pMin.y &&
        p.y < b.pMax.y&& p.z >= b.pMin.z && p.z < b.pMax.z);
}

inline Bounds3 Expand(const Bounds3& b, float delta)
{
    return Bounds3(b.pMin - Vector3f(delta, delta, delta),
        b.pMax + Vector3f(delta, delta, delta));
}

// Minimum squared distance from point to box; returns zero if point is inside.
inline float DistanceSquared(const Point3f& p, const Bounds3& b)
{
    float dx = std::max({ float(0), b.pMin.x - p.x, p.x - b.pMax.x });
    float dy = std::max({ float(0), b.pMin.y - p.y, p.y - b.pMax.y });
    float dz = std::max({ float(0), b.pMin.z - p.z, p.z - b.pMax.z });
    return dx * dx + dy * dy + dz * dz;
}

inline float Distance(const Point3f& p, const Bounds3& b)
{
    return std::sqrt(DistanceSquared(p, b));
}

inline bool Bounds3::IntersectP(const Ray& ray, float* hitt0, float* hitt1) const
{
    float t0 = 0;
    float t1 = ray.tMax;

    for(int i = 0; i < 3; i++)
    {
        // Update interval for ith bounding box slab
        float invRayDir = 1 / ray.direction[i];
        float tNear = (pMin[i] - ray.origin[i]) * invRayDir;
        float tFar = (pMax[i] - ray.origin[i]) * invRayDir;
        if(tNear > tFar) std::swap(tNear, tFar);

        t0 = (tNear > t0) ? tNear : t0;
        t1 = (tFar < t1) ? tFar : t1;
        if(t0 > t1) return false;
    }

    if(hitt0) *hitt0 = t0;
    if(hitt1) *hitt1 = t1;

    return true;
}
