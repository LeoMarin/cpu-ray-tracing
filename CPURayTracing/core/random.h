#pragma once

#include <random>
#include "geometry.h"

inline float RandomFloat()
{
	return (rand() % 10000) * 0.0001f;
}

inline Vector3f RandomPointInUnitSphere()
{
	Vector3f p;
	do
	{
		p = Vector3f(RandomFloat(), RandomFloat(), RandomFloat());
	} while (p.LengthSquared() >= 1);
	return p;
}

inline Vector3f RandomPointInUnitDisk()
{
	Vector3f point;
	do
	{
		point = Vector3f(RandomFloat(), RandomFloat(), 0);
	} while (Dot(point, point) >= 1);
	return point;
}

inline Vector3f RandomVector()
{
	return Vector3f(RandomFloat(), RandomFloat(), RandomFloat());
}