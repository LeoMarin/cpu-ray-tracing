#pragma once

#include <vector>

// shape includes
#include "shape.h"
#include "sphere.h"
#include "plane.h"
#include "cylinder.h"
#include "cone.h"
#include "cube.h"
#include "triangle.h"
#include "quad.h"

// light include
#include "light.h"
#include "point.h"
#include "directional.h"
#include "area.h"

// acellerator includes
#include "accelerator.h"
#include "bvh.h"
#include "kd.h"


void CornellBox(std::vector<Light*>& lights, std::vector<Shape*>& shapes);
void LightBalls(std::vector<Light*>& lights, std::vector<Shape*>& shapes);
void ColoredLights(std::vector<Light*>& lights, std::vector<Shape*>& shapes);
void Cubes(std::vector<Light*>& lights, std::vector<Shape*>& shapes);
void RandomBalls(std::vector<Light*>& lights, std::vector<Shape*>& shapes, float f);

// Scene Declaration
class Scene
{
public:
    Scene(float f);
    ~Scene()
    {
        for (auto& mesh : meshes)
            delete mesh;
    }
    bool Intersect(const Ray& ray, SurfaceInteraction* isect) const;

public:
    std::vector<Light*> lights;

private:
    Accelerator* accelerator = nullptr;
    std::vector<TriangleMesh*> meshes;
    std::vector<Shape*> shapes;
};

Scene::Scene()
{
    Cubes(lights, shapes);
    accelerator = new BVH(shapes);
}

// Scene Method Definitions
bool Scene::Intersect(const Ray& ray, SurfaceInteraction* isect) const
{
    if (accelerator)
        return accelerator->Intersect(ray, isect);
}

void RandomBalls(std::vector<Light*>& lights, std::vector<Shape*>& shapes, float f)
{
    Transform T1;
    f *= 0.8;

    for (int i = 0; i < 0; i++)
    {
        float r = RandomFloat() + 0.5f;
        T1 = Translate(Vector3f(RandomFloat() * 60 - 30, r, RandomFloat() * 60 - 30));
        Vector3f color = RandomVector();
        shapes.push_back(new Sphere(r, T1, new Emitting(color)));
        lights.push_back(new Area(shapes[i], color, RandomFloat() * 2));
    }

    for (int i = 0; i < f; i++)
    {
        float r = RandomFloat() + 0.5f;
        T1 = Translate(Vector3f(RandomFloat() * 60 - 30, r + RandomFloat() * 60 - 30, RandomFloat() * 60 - 30));
        Vector3f color = RandomVector();

        float random = RandomFloat();
        if (random < 0.6)
            shapes.push_back(new Sphere(r, T1, new Diffuse(color)));
        else if (random < 0.9)
            shapes.push_back(new Sphere(r, T1, new Metalic(color, RandomFloat())));
        else
            shapes.push_back(new Sphere(r, T1, new Dielectric(Vector3f(1), 1.5f)));
    }
}

void CornellBox(std::vector<Light*>& lights, std::vector<Shape*>& shapes)
{
    // Camera:
    // theta = 60
    // int nx = 200;
    // int ny = 250;
    //Vector3f lookFrom(0, 2.5, 6);
    //Vector3f lookAt(0, 2.5, 0);

    Transform T7 = Translate(Vector3f(0, 4.99f, 0.5f)) * Scale(0.5f, 1, 0.5f);
    shapes.push_back(new Quad(T7, new Emitting(Vector3f(1))));
    lights.push_back(new Area(shapes[0], Vector3f(1), 1));

    // left wall
    Transform T1 = Translate(Vector3f(-3, 0, 0)) * Scale(1, 10, 10);
    shapes.push_back(new Cube(T1, new Diffuse(Vector3f(0.3f, 0, 0))));
    // right wall
    Transform T2 = Translate(Vector3f(3, 0, 0)) * Scale(1, 10, 10);
    shapes.push_back(new Cube(T2, new Diffuse(Vector3f(0, 0.3f, 0))));
    // floor
    Transform T3 = Translate(Vector3f(0, -1, 0)) * Scale(10, 1, 10);
    shapes.push_back(new Cube(T3, new Diffuse(Vector3f(0.6f))));
    // back wall
    Transform T4 = Translate(Vector3f(0, 0, 7)) * Scale(10, 10, 1);
    shapes.push_back(new Cube(T4, new Diffuse(Vector3f(0.6f))));
    // front wall
    Transform T5 = Translate(Vector3f(0, 0, -3)) * Scale(10, 10, 1);
    shapes.push_back(new Cube(T5, new Diffuse(Vector3f(0.6f))));
    // ceiling
    Transform T6 = Translate(Vector3f(0, 6, 0)) * Scale(10, 1, 10);
    shapes.push_back(new Cube(T6, new Diffuse(Vector3f(0.6f))));

    Transform T8 = Translate(Vector3f(-0.5f, 1, -1)) * Scale(0.7f, 2, 0.7f) * RotateY(20);
    shapes.push_back(new Cube(T8, new Diffuse(Vector3f(0.8f))));

    Transform T9 = Translate(Vector3f(0.9f, 0.7f, 0.7f)) * Scale(0.7f, 0.7f, 0.7f) * RotateY(-25);
    shapes.push_back(new Cube(T9, new Diffuse(Vector3f(0.8f))));
}

void LightBalls(std::vector<Light*>& lights, std::vector<Shape*>& shapes)
{

    //lights.push_back(new DirectionalLight(Vector3f(-3, -2, 0), Vector3f(1), 2));

    //lights.push_back(new PointLight(Point3f(3, 6, 0), Vector3f(1), 10));

    Transform T1 = Translate(Vector3f(1, 3, 0));
    shapes.push_back(new Sphere(0.5f, T1, new Emitting(Vector3f(1))));
    lights.push_back(new Area(shapes[0], Vector3f(Vector3f(1)), 3));


    //T1 = Translate(Vector3f(-3, -0.8f, 3));
    //shapes.push_back(new Sphere(0.2f, T1, new Emitting(Vector3f(1, 1, 0))));
    //lights.push_back(new Area(shapes[1], Vector3f(Vector3f(1, 1, 0)), 0.6f));

    T1 = Translate(Vector3f(0, 0, 0));
    shapes.push_back(new Sphere(1, T1, new Metalic(Vector3f(0.7f), 0.3f)));

    T1 = Translate(Vector3f(0, -1001, 0));
    shapes.push_back(new Sphere(1000, T1, new Diffuse(Vector3f(0.05f))));

    T1 = Translate(Vector3f(4, 0, -3));
    shapes.push_back(new Sphere(1, T1, new Diffuse(Vector3f(0, 0, 0.5f))));

    T1 = Translate(Vector3f(4, 0, 2));
    shapes.push_back(new Sphere(1, T1, new Diffuse(Vector3f(0.3f, 0.3f, 0))));

    T1 = Translate(Vector3f(-2, 0, -3));
    shapes.push_back(new Sphere(1, T1, new Diffuse(Vector3f(0.5f, 0.2f, 0.2f))));

    T1 = Translate(Vector3f(-4, 0, 1));
    shapes.push_back(new Sphere(1, T1, new Diffuse(Vector3f(0.3f, 0, 0.5f))));
}

void ColoredLights(std::vector<Light*>& lights, std::vector<Shape*>& shapes)
{
    Transform T1 = Translate(Vector3f(0, -1001, 0));
    shapes.push_back(new Sphere(1000, T1, new Diffuse(Vector3f(0.05f))));

    for(size_t i = 0; i < 20; i++)
    {
        T1 = Translate(Vector3f(RandomFloat() * 20 - 10, -0.5f, RandomFloat() * 16 - 10));
        float r = RandomFloat();
        float g = RandomFloat();
        float b = RandomFloat();
        shapes.push_back(new Sphere(0.5f, T1, new Emitting(Vector3f(r, g, b))));
        lights.push_back(new Area(shapes[i+1], Vector3f(Vector3f(r, g, b)), 1));
    }
}

void Cubes(std::vector<Light*>& lights, std::vector<Shape*>& shapes)
{
    Transform T3 = Translate(Vector3f(0, -1001, 0));
    shapes.push_back(new Sphere(1000, T3, new Diffuse(Vector3f(0.3f))));

    Transform T4 = Translate(Vector3f(5, 0, 5)) * RotateY(0) * Scale(0.1f, 1, 1);
    shapes.push_back(new Cube(T4, new Metalic(Vector3f(0.0f, 0.6f, 0.3f), 0.7f)));

    Transform T5 = Translate(Vector3f(0, 0, 0));
    shapes.push_back(new Cube(T5, new Metalic(Vector3f(0.5f, 0.2f, 0.5f), 0.5f)));

    Transform T6 = Translate(Vector3f(-4, -0.9f, 5)) * Scale(1, 0.1f, 0.6f);
    shapes.push_back(new Cube(T6, new Diffuse(Vector3f(0.6f, 0.3f, 0.2f))));

    Transform T7 = Translate(Vector3f(6, 0, -8));
    shapes.push_back(new Cube(T7, new Diffuse(Vector3f(0.15f))));

    Transform T8 = Translate(Vector3f(-3, -0.5f, 1)) * Scale(1, 0.5f, 0.2f) * RotateY(65);
    shapes.push_back(new Cube(T8, new Diffuse(Vector3f(0.2f, 0.6f, 0.4f))));

    Transform T9 = Translate(Vector3f(7, -0.8f, -15)) * RotateY(-50) * Scale(0.2f, 0.2f, 20);
    shapes.push_back(new Cube(T9, new Metalic(Vector3f(0.9f), 0.6f)));

    Transform T10 = Translate(Vector3f(-10, 0, -18)) * Scale(1, 1, 1);;
    shapes.push_back(new Cube(T10, new Diffuse(Vector3f(0.5f, 0.2f, 0.2f))));

    Transform T11 = Translate(Vector3f(-7, 0, -18)) * Scale(1, 1.8f, 1);;
    shapes.push_back(new Cube(T11, new Diffuse(Vector3f(0.5f, 0.5f, 0.2f))));

    Transform T12 = Translate(Vector3f(-4, 0, -18)) * Scale(1, 2.6f, 1);;
    shapes.push_back(new Cube(T12, new Diffuse(Vector3f(0.2f, 0.5f, 0.2f))));

    Transform T13 = Translate(Vector3f(-1, 0, -18)) * Scale(1, 3.4f, 1);;
    shapes.push_back(new Cube(T13, new Diffuse(Vector3f(0.2f, 0.5f, 0.5f))));

    Transform T14 = Translate(Vector3f(2, 0, -18)) * Scale(1, 4.2f, 1);
    shapes.push_back(new Cube(T14, new Diffuse(Vector3f(0.2f, 0.2f, 0.5f))));

}
