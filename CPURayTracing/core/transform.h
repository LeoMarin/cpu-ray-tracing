#pragma once

#include <cmath>
#include <iostream>

#include "geometry.h"

/*
/
/ *** Matrix4x4 ***
/
*/

struct Matrix4x4
{
    // Matrix public methods
    Matrix4x4();
    Matrix4x4(float mat[4][4]);
    Matrix4x4(float t00, float t01, float t02, float t03, float t10, float t11,
        float t12, float t13, float t20, float t21, float t22, float t23,
        float t30, float t31, float t32, float t33);
    bool operator==(const Matrix4x4& m2) const;
    bool operator!=(const Matrix4x4& m2) const;

    friend Matrix4x4 Transpose(const Matrix4x4& m);
    static Matrix4x4 Mul(const Matrix4x4& m1, const Matrix4x4& m2);
    friend Matrix4x4 Inverse(const Matrix4x4& m);

    // Matrix public data
    float m[4][4];
};

/*
/
/ *** Transofrm ***
/
*/

class Transform
{
public:
    // Transform public methods
    Transform() {}    
    Transform(const float mat[4][4]) {
        m = Matrix4x4(mat[0][0], mat[0][1], mat[0][2], mat[0][3], mat[1][0],
            mat[1][1], mat[1][2], mat[1][3], mat[2][0], mat[2][1],
            mat[2][2], mat[2][3], mat[3][0], mat[3][1], mat[3][2],
            mat[3][3]);
        mInv = Inverse(m);
    }
    Transform(const Matrix4x4 &m) : m(m), mInv(Inverse(m)) {}
    Transform(const Matrix4x4 &m, const Matrix4x4 &mInv) : m(m), mInv(mInv) {}

    friend Transform Inverse(const Transform& t) { return Transform(t.mInv, t.m); }
    friend Transform Transpose(const Transform& t) { return Transform(Transpose(t.m), Transpose(t.mInv)); }
    bool operator==(const Transform& t) const { return t.m == m && t.mInv == mInv; }
    bool operator!=(const Transform& t) const { return t.m != m || t.mInv != mInv; }

    bool operator<(const Transform& t2) const;
    bool IsIdentity() const;

    const Matrix4x4 &GetMatrix() const { return m; }
    const Matrix4x4 &GetInverseMatrix() const { return mInv; }

    bool SwapsHandedness() const;

    Transform operator*(const Transform& t2) const;

    inline Point3f operator()(const Point3f& p) const;
    inline Vector3f operator()(const Vector3f& v) const;
    inline Normal3f operator()(const Normal3f&) const;
    inline Ray operator()(const Ray& r) const;
    Bounds3 operator()(const Bounds3& b) const;
    // SurfaceInteraction operator()(const SurfaceInteraction &si) const;
    inline Point3f operator()(const Point3f& pt, Vector3f* absError) const;
    inline Point3f operator()(const Point3f& p, const Vector3f& pError, Vector3f* pTransError) const;
    inline Vector3f operator()(const Vector3f& v, Vector3f* vTransError) const;
    inline Vector3f operator()(const Vector3f& v, const Vector3f& vError, Vector3f* vTransError) const;
    inline Ray operator()(const Ray& r, Vector3f* oError, Vector3f* dError) const;
    inline Ray operator()(const Ray& r, const Vector3f& oErrorIn, const Vector3f& dErrorIn, Vector3f* oErrorOut, Vector3f* dErrorOut) const;

private:
    // Transform private data
    Matrix4x4 m, mInv;
};

Transform Translate(const Vector3f& delta);
Transform Scale(float x, float y, float z);
Transform RotateX(float theta);
Transform RotateY(float theta);
Transform RotateZ(float theta);
Transform Rotate(float theta, const Vector3f& axis);
Transform LookAt(const Point3f& pos, const Point3f& look, const Vector3f& up);
Transform Orthographic(float znear, float zfar);
Transform Perspective(float fov, float znear, float zfar);
bool SolveLinearSystem2x2(const float A[2][2], const float B[2], float* x0, float* x1);


// Transform Inline Functions
inline Point3f Transform::operator()(const Point3f& p) const
{
    float x = p.x, y = p.y, z = p.z;
    float xp = m.m[0][0] * x + m.m[0][1] * y + m.m[0][2] * z + m.m[0][3];
    float yp = m.m[1][0] * x + m.m[1][1] * y + m.m[1][2] * z + m.m[1][3];
    float zp = m.m[2][0] * x + m.m[2][1] * y + m.m[2][2] * z + m.m[2][3];
    float wp = m.m[3][0] * x + m.m[3][1] * y + m.m[3][2] * z + m.m[3][3];
    if (wp == 1)
        return Point3f(xp, yp, zp);
    else
        return Point3f(xp, yp, zp) / wp;
}

inline Vector3f Transform::operator()(const Vector3f& v) const
{
    float x = v.x, y = v.y, z = v.z;
    return Vector3f(m.m[0][0] * x + m.m[0][1] * y + m.m[0][2] * z,
        m.m[1][0] * x + m.m[1][1] * y + m.m[1][2] * z,
        m.m[2][0] * x + m.m[2][1] * y + m.m[2][2] * z);
}

inline Normal3f Transform::operator()(const Normal3f& n) const
{
    float x = n.x, y = n.y, z = n.z;
    return Normal3f(mInv.m[0][0] * x + mInv.m[1][0] * y + mInv.m[2][0] * z,
        mInv.m[0][1] * x + mInv.m[1][1] * y + mInv.m[2][1] * z,
        mInv.m[0][2] * x + mInv.m[1][2] * y + mInv.m[2][2] * z);
}

inline Ray Transform::operator()(const Ray& r) const
{
    Vector3f oError;
    Point3f o = (*this)(r.origin, &oError);
    Vector3f d = (*this)(r.direction);
    // Offset ray origin to edge of error bounds and compute _tMax_
    float lengthSquared = d.LengthSquared();
    float tMax = r.tMax;
    if (lengthSquared > 0) {
        float dt = Dot(Abs(d), oError) / lengthSquared;
        o += d * dt;
        tMax -= dt;
    }
    return Ray(o, d, tMax);
}

inline Bounds3 Transform::operator()(const Bounds3& b) const
{
    const Transform& M = *this;
    Bounds3 ret(M(Point3f(b.pMin.x, b.pMin.y, b.pMin.z)));
    ret = Union(ret, M(Point3f(b.pMax.x, b.pMin.y, b.pMin.z)));
    ret = Union(ret, M(Point3f(b.pMin.x, b.pMax.y, b.pMin.z)));
    ret = Union(ret, M(Point3f(b.pMin.x, b.pMin.y, b.pMax.z)));
    ret = Union(ret, M(Point3f(b.pMin.x, b.pMax.y, b.pMax.z)));
    ret = Union(ret, M(Point3f(b.pMax.x, b.pMax.y, b.pMin.z)));
    ret = Union(ret, M(Point3f(b.pMax.x, b.pMin.y, b.pMax.z)));
    ret = Union(ret, M(Point3f(b.pMax.x, b.pMax.y, b.pMax.z)));
    return ret;
}

inline Point3f Transform::operator()(const Point3f& p, Vector3f* pError) const
{
    float x = p.x, y = p.y, z = p.z;
    // Compute transformed coordinates from point _pt_
    float xp = (m.m[0][0] * x + m.m[0][1] * y) + (m.m[0][2] * z + m.m[0][3]);
    float yp = (m.m[1][0] * x + m.m[1][1] * y) + (m.m[1][2] * z + m.m[1][3]);
    float zp = (m.m[2][0] * x + m.m[2][1] * y) + (m.m[2][2] * z + m.m[2][3]);
    float wp = (m.m[3][0] * x + m.m[3][1] * y) + (m.m[3][2] * z + m.m[3][3]);

    // Compute absolute error for transformed point
    float xAbsSum = (std::abs(m.m[0][0] * x) + std::abs(m.m[0][1] * y) +
        std::abs(m.m[0][2] * z) + std::abs(m.m[0][3]));
    float yAbsSum = (std::abs(m.m[1][0] * x) + std::abs(m.m[1][1] * y) +
        std::abs(m.m[1][2] * z) + std::abs(m.m[1][3]));
    float zAbsSum = (std::abs(m.m[2][0] * x) + std::abs(m.m[2][1] * y) +
        std::abs(m.m[2][2] * z) + std::abs(m.m[2][3]));
    *pError = gamma(3) * Vector3f(xAbsSum, yAbsSum, zAbsSum);
    if (wp == 1)
        return Point3f(xp, yp, zp);
    else
        return Point3f(xp, yp, zp) / wp;
}

inline Point3f Transform::operator()(const Point3f& pt, const Vector3f& ptError, Vector3f* absError) const
{
    float x = pt.x, y = pt.y, z = pt.z;
    float xp = (m.m[0][0] * x + m.m[0][1] * y) + (m.m[0][2] * z + m.m[0][3]);
    float yp = (m.m[1][0] * x + m.m[1][1] * y) + (m.m[1][2] * z + m.m[1][3]);
    float zp = (m.m[2][0] * x + m.m[2][1] * y) + (m.m[2][2] * z + m.m[2][3]);
    float wp = (m.m[3][0] * x + m.m[3][1] * y) + (m.m[3][2] * z + m.m[3][3]);
    absError->x =
        (gamma(3) + (float)1) *
        (std::abs(m.m[0][0]) * ptError.x + std::abs(m.m[0][1]) * ptError.y +
            std::abs(m.m[0][2]) * ptError.z) +
        gamma(3) * (std::abs(m.m[0][0] * x) + std::abs(m.m[0][1] * y) +
            std::abs(m.m[0][2] * z) + std::abs(m.m[0][3]));
    absError->y =
        (gamma(3) + (float)1) *
        (std::abs(m.m[1][0]) * ptError.x + std::abs(m.m[1][1]) * ptError.y +
            std::abs(m.m[1][2]) * ptError.z) +
        gamma(3) * (std::abs(m.m[1][0] * x) + std::abs(m.m[1][1] * y) +
            std::abs(m.m[1][2] * z) + std::abs(m.m[1][3]));
    absError->z =
        (gamma(3) + (float)1) *
        (std::abs(m.m[2][0]) * ptError.x + std::abs(m.m[2][1]) * ptError.y +
            std::abs(m.m[2][2]) * ptError.z) +
        gamma(3) * (std::abs(m.m[2][0] * x) + std::abs(m.m[2][1] * y) +
            std::abs(m.m[2][2] * z) + std::abs(m.m[2][3]));
    if (wp == 1.)
        return Point3f(xp, yp, zp);
    else
        return Point3f(xp, yp, zp) / wp;
}

inline Vector3f Transform::operator()(const Vector3f& v, Vector3f* absError) const
{
    float x = v.x, y = v.y, z = v.z;
    absError->x =
        gamma(3) * (std::abs(m.m[0][0] * v.x) + std::abs(m.m[0][1] * v.y) +
            std::abs(m.m[0][2] * v.z));
    absError->y =
        gamma(3) * (std::abs(m.m[1][0] * v.x) + std::abs(m.m[1][1] * v.y) +
            std::abs(m.m[1][2] * v.z));
    absError->z =
        gamma(3) * (std::abs(m.m[2][0] * v.x) + std::abs(m.m[2][1] * v.y) +
            std::abs(m.m[2][2] * v.z));
    return Vector3f(m.m[0][0] * x + m.m[0][1] * y + m.m[0][2] * z,
        m.m[1][0] * x + m.m[1][1] * y + m.m[1][2] * z,
        m.m[2][0] * x + m.m[2][1] * y + m.m[2][2] * z);
}

inline Vector3f Transform::operator()(const Vector3f& v, const Vector3f& vError, Vector3f* absError) const
{
    float x = v.x, y = v.y, z = v.z;
    absError->x =
        (gamma(3) + (float)1) *
        (std::abs(m.m[0][0]) * vError.x + std::abs(m.m[0][1]) * vError.y +
            std::abs(m.m[0][2]) * vError.z) +
        gamma(3) * (std::abs(m.m[0][0] * v.x) + std::abs(m.m[0][1] * v.y) +
            std::abs(m.m[0][2] * v.z));
    absError->y =
        (gamma(3) + (float)1) *
        (std::abs(m.m[1][0]) * vError.x + std::abs(m.m[1][1]) * vError.y +
            std::abs(m.m[1][2]) * vError.z) +
        gamma(3) * (std::abs(m.m[1][0] * v.x) + std::abs(m.m[1][1] * v.y) +
            std::abs(m.m[1][2] * v.z));
    absError->z =
        (gamma(3) + (float)1) *
        (std::abs(m.m[2][0]) * vError.x + std::abs(m.m[2][1]) * vError.y +
            std::abs(m.m[2][2]) * vError.z) +
        gamma(3) * (std::abs(m.m[2][0] * v.x) + std::abs(m.m[2][1] * v.y) +
            std::abs(m.m[2][2] * v.z));
    return Vector3f(m.m[0][0] * x + m.m[0][1] * y + m.m[0][2] * z,
        m.m[1][0] * x + m.m[1][1] * y + m.m[1][2] * z,
        m.m[2][0] * x + m.m[2][1] * y + m.m[2][2] * z);
}

inline Ray Transform::operator()(const Ray& r, Vector3f* oError, Vector3f* dError) const
{
    Point3f o = (*this)(r.origin, oError);
    Vector3f d = (*this)(r.direction, dError);
    float tMax = r.tMax;
    float lengthSquared = d.LengthSquared();
    if (lengthSquared > 0) {
        float dt = Dot(Abs(d), *oError) / lengthSquared;
        o += d * dt;
        //        tMax -= dt;
    }
    return Ray(o, d, tMax);
}

inline Ray Transform::operator()(const Ray& r, const Vector3f& oErrorIn, const Vector3f& dErrorIn, Vector3f* oErrorOut, Vector3f* dErrorOut) const
{
    Point3f o = (*this)(r.origin, oErrorIn, oErrorOut);
    Vector3f d = (*this)(r.direction, dErrorIn, dErrorOut);
    float tMax = r.tMax;
    float lengthSquared = d.LengthSquared();
    if (lengthSquared > 0) {
        float dt = Dot(Abs(d), *oErrorOut) / lengthSquared;
        o += d * dt;
        //        tMax -= dt;
    }
    return Ray(o, d, tMax);
}