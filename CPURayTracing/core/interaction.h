#pragma once

#include "geometry.h"

class Material;

// Interaction Declarations
class Interaction
{
public:
    // Interaction Public Methods
    Interaction() {}
    Interaction(const Point3f& point, const Normal3f& normal)
        :point(point), normal(normal)
    {}
    ~Interaction() {}

    // Interaction Public Data
    Point3f point;
    Normal3f normal;
};


// SurfaceInteraction Declarations
class SurfaceInteraction : public Interaction
{
public:
    // SurfaceInteraction Public Methods
    SurfaceInteraction() : material(nullptr) {}
    SurfaceInteraction(const Point3f& point, const Normal3f& normal, Material* material)
        :Interaction(point, normal), material(material)
    {}
    ~SurfaceInteraction() {}

    Material* material;
};