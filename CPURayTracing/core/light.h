#pragma once

#include "geometry.h"
#include "interaction.h"

class Light
{
public:
    virtual Vector3f Le(const Ray& shadowRay) const = 0;
    virtual Vector3f SampleLi(const Ray& shadowRay, SurfaceInteraction* isect, SurfaceInteraction* shadowIsect) const = 0;
    virtual Point3f Position() const = 0;
    virtual Material* LightMaterial() const { return nullptr; }
};
