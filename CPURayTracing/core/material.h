#pragma once

#include "interaction.h"
#include "random.h"

class SurfaceInteraction;

// Material Declaration
class Material
{
public:
	// Material Interface
	Material() {};
	virtual bool Scatter(const Ray& ray, SurfaceInteraction* isect, Ray& scatterRay) const = 0;
	virtual Vector3f Albedo() const = 0;
	virtual Vector3f Emitted() const { return Vector3f(); }
};

// material that scatters light
class Diffuse : public Material
{
public:
	// Diffuse Public Methods
	Diffuse(const Vector3f& albedo) :albedo(albedo) {}

	bool Scatter(const Ray& ray, SurfaceInteraction* isect, Ray& scatterRay) const
	{
		// scatter rays in random direction arround normal
		scatterRay = Ray(isect->point, Vector3f(isect->normal) + RandomPointInUnitSphere());
		return true;
	}

	Vector3f Albedo() const
	{
		return albedo;
	}
private:
	// Diffuse Private Data

	Vector3f albedo; // color of the object
};

// material that reflects light
class Metalic : public Material
{
public:
	// Metalic Public Methods
	Metalic(const Vector3f& albedo, float fuzzines) :albedo(albedo), fuzzines(fuzzines) {}

	bool Scatter(const Ray& ray, SurfaceInteraction* isect, Ray& scatterRay) const
	{
		// Scatter rays like a mirror
		Vector3f reflectedDirection = Reflect(ray.direction, isect->normal);
		scatterRay = Ray(isect->point, reflectedDirection + RandomPointInUnitSphere() * fuzzines);

		//return true;
		return Dot(scatterRay.direction, isect->normal) > 0;
	}

	Vector3f Albedo() const
	{
		return albedo;
	}
private:
	// Metalic Private Data
	Vector3f albedo; // color of the object
	float fuzzines;
};

// material that refracts light
class Dielectric : public Material
{
public:
	Dielectric(const Vector3f& albedo, float refractionIndex) :albedo(albedo), refractionIndex(refractionIndex) {}

	bool Scatter(const Ray& ray, SurfaceInteraction* isect, Ray& scatterRay) const
	{
		Normal3f otwardNormal;
		Vector3f reflectedDirection = Reflect(ray.direction, isect->normal);
		Vector3f refractedDirection;

		// n2/n1
		float refractionQuotient;

		// cosine used in Shlick aproximation
		float cosine;
		float reflectionProbability;

		// ray is coming from outside of the object
		if (Dot(ray.direction, isect->normal) > 0)
		{
			otwardNormal = -isect->normal;
			// n2/n1 = refractionIndex/1
			refractionQuotient = refractionIndex;

			cosine = Dot(ray.direction, isect->normal) * refractionIndex;
		}
		// ray is coming from inside the object
		else
		{
			otwardNormal = isect->normal;
			// n2/n1 = 1/refractionIndex
			refractionQuotient = 1 / refractionIndex;

			cosine = -Dot(ray.direction, isect->normal);
		}

		// check if ray will refract
		if (Refract(ray, otwardNormal, refractionQuotient, refractedDirection))
		{
			// if ray is refracted calculate Schlick's Approximation
			reflectionProbability = SchlickApproximation(refractionIndex, cosine);
		}
		else
		{
			// if ray is not refracted, create scatter ray from reflection
			scatterRay = Ray(isect->point, reflectedDirection);
			return true;
		}

		// generate random number to determine if ray is reflected or reftacted
		if ((rand() % 100) / (float)100 < reflectionProbability)
		{
			// ray is reflected
			scatterRay = Ray(isect->point, reflectedDirection);
		}
		else
		{
			// ray is refracted
			scatterRay = Ray(isect->point, refractedDirection);
		}
		return true;
	}

	Vector3f Albedo() const
	{
		return albedo;
	}

private:
	bool Refract(const Ray& ray, const Normal3f& normal, float refractionQuotient, Vector3f& refractedDirection) const
	{
		// n1 sin(fi1) = n2 sin (fi2)
		// sin(fi2) = n2/n1 sqrt(1 - cos2(fi1))
		// cos(fi2) = sqrt(1 - sin2(fi2))
		// cos(fi2) = sqrt(1 - (n2/n1)2 (1 - cos2(fi1))
		float cosFi1 = Dot(ray.direction, normal);
		float discriminant = 1 - refractionQuotient * refractionQuotient * (1 - cosFi1 * cosFi1);
		if (discriminant > 0)
		{
			refractedDirection = (ray.direction - Vector3f(normal * cosFi1)) * refractionQuotient - Vector3f(normal) * sqrt(discriminant);
			return true;
		}
		else
		{
			return false;
		}
	}

	// approximates contribution of reflection when light hits refracting surface
	float SchlickApproximation(float refractionIndex, float cosineFi) const
	{
		// R = R0 + (1 - R0)(1 - cosFi)^5
		// R0 = ((n1 - n2) / (n1 + n2))^2
		float r0 = (1 - refractionIndex) / (1 + refractionIndex);
		r0 = r0 * r0;
		return r0 + (1 - r0) * (float)pow((1 - cosineFi), 5);
	}


private:
	Vector3f albedo;
	float refractionIndex;
};

class Emitting : public Material
{
public:
	Emitting(const Vector3f& albedo) :albedo(albedo) {}

	virtual bool Scatter(const Ray& ray, SurfaceInteraction* isect, Ray& scatterRay) const
	{
		return false;
	}

	virtual Vector3f Albedo() const
	{
		return albedo;
	}

	virtual Vector3f Emitted() const
	{
		return albedo;
	}
private:
	Vector3f albedo;
};
