#include <iostream>
#include <fstream>
#include <thread>

#include <math.h>
#include <time.h>

#include "camera.h"
#include "scene.h"
#include "random.h"

Vector3f Color(const Ray& ray, Scene* scene, int depth = 0)
{
	SurfaceInteraction isect;
	if (scene->Intersect(ray, &isect))
	{
		// Color by normal
		//return 0.5f * ((Vector3f(isect.normal) + Vector3f(1, 1, 1)));

		Vector3f albedo = isect.material->Albedo();
		Vector3f emitted = isect.material->Emitted();
		Ray scatterRay;
		if (depth < 50 && isect.material->Scatter(ray, &isect, scatterRay))
		{
			SurfaceInteraction shadowIsect;
			Vector3f Li;
			for(auto& light : scene->lights)
			{
				Vector3f shadowRayDirection = Vector3f(light->Position()) - Vector3f(isect.point);
				Ray shadowRay = Ray(isect.point + Vector3f(isect.normal) * ShadowEpsilon, shadowRayDirection);
				scene->Intersect(shadowRay, &shadowIsect);
				if(shadowRay.tMax > shadowRayDirection.Length() ||
					shadowIsect.material == light->LightMaterial())
				{
					Li += light->SampleLi(shadowRay, &isect, &shadowIsect);
				}
			}
			return Li + emitted + albedo * Color(scatterRay, scene, depth + 1);
		}
		return emitted;
	}

	//return Vector3f(0);
	// get value of y and clamp it to 0 < t < 1
	float t = 0.5f * (ray.direction.y + 1.f);
	// calculate color using linear interpolation (lerp)
	// blendedValue = (1-t)*startValue + t*endValue
	Vector3f color = Vector3f(1, 1, 1) * (1 - t) + Vector3f(0.5f, 0.7f, 1.0f) * t;
	return color;
}

void ThreadOperation(Vector3f* fb, const Camera& camera, Scene* scene, int startX, int endX)
{
	const size_t width = camera.width;
	const size_t height = camera.height;

	// sampling for antialiasing
	const size_t numberOfSamples = 100;

	// cast ray for every pixel final image
	for(int j = height - 1; j >= 0; j--)
	{
		for(int i = endX; i < width; i+=startX)
		{
			Vector3f color;

			// cast multiple rays for single pixel
			for(size_t s = 0; s < numberOfSamples; s++)
			{
				// calculate offset for current ray
				float u = (float)(i + RandomFloat()) / width;
				float v = (float)(j + RandomFloat()) / height;

				// get color of current pixel using ray tracing
				color += Color(camera.GetRay(u, v), scene);
			}

			// get average color of current pixel
			color = color / (float)numberOfSamples;
			color = Clamp(color);

			// gama correlation
			color = Vector3f(pow(color.x, 0.4545f), pow(color.y, 0.4545f), pow(color.z, 0.4545f));

			// save current pixel color to FB
			size_t pixel_index = j * width + i;
			fb[pixel_index] = color;
		}
	}
}

void Render(Vector3f* fb, const Camera& camera, Scene* scene)
{
	const int number_of_usable_threads = std::thread::hardware_concurrency() - 2;
	
	float widthPerThread = (float)camera.width / number_of_usable_threads;

	std::vector<std::thread> threads;
	threads.reserve(number_of_usable_threads);

	for(int i = 0; i < number_of_usable_threads; i++)
		threads.push_back(std::thread(ThreadOperation, fb, std::ref(camera), scene, number_of_usable_threads, i));

	for(auto& thread : threads)
	{
		thread.join();
	}

}

void WriteImage(Vector3f* fb, int nx, int ny)
{
	// Output FB as Image
	std::cout << "Writing image to file..." << std::endl;
	// open .ppm file
	std::ofstream ofs;
	ofs.open("./CPUimages/out.ppm");
	ofs << "P3\n" << nx << " " << ny << "\n255\n";

	for (int j = ny-1; j >= 0; j--) 
 	{
		for (int i = 0; i < nx; i++) 
		{
			size_t pixel_index = j * nx + i;
			// scale color to 0-255 for rgb values
			int ir = int(255.99 * fb[pixel_index].x);
			int ig = int(255.99 * fb[pixel_index].y);
			int ib = int(255.99 * fb[pixel_index].z);
			// write color to output file
			ofs << ir << " " << ig << " " << ib << "\n";
		}
	}

	// close .ppm file
	ofs.close();
}


int main() 
{
	srand(time(NULL));
	std::cout << "Setting up scene..\n";

	// Instanciranje frame buffera
    int nx = 600;
    int ny = 400;
	int num_pixels = nx*ny;
	Vector3f* fb = new Vector3f[num_pixels];

    // Instanciranje Camera objekta
	Vector3f lookFrom(0, 4, 10);
	Vector3f lookAt(0, 0, 0);
    Camera camera(lookFrom, lookAt, nx, ny);

    // Instanciranje Scene objekta
	Scene* scene = new Scene();

	std::cout << "Rendering a " << nx << "x" << ny << " image\n";

    // start timing
    clock_t start, stop;
    start = clock();

    // Render petlja
    Render(fb, camera, scene);

    // stop timing
    stop = clock();
    double timer_seconds = ((double)(stop - start)) / CLOCKS_PER_SEC;
    std::cout << "took " << timer_seconds << " seconds.\n";

    // Zapisivanje frame buffera u sliku
    WriteImage(fb, nx, ny);
}
