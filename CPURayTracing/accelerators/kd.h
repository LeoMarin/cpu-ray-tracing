#pragma once

#include <vector>
#include "accelerator.h"
#include "shape.h"

struct KDNode
{
	KDNode(std::vector<Shape*>& shapes, int maxShapesInBox, int depth = 0)
	{
		// check if AABB contains more than permited number of shapes
		if (shapes.size() <= maxShapesInBox || depth > 10)
		{
			// create leaf node
			leftChild = nullptr;
			rightChild = nullptr;

			leafShapes = shapes;
		}
		else
		{
			// find largest axis
			int split = depth % 3;
			dimSplit = split;
			// sort on that axis
			std::sort(shapes.begin(), shapes.end() - 1, [split](const Shape* lhs, const Shape* rhs) {
				return lhs->WorldBound().Center()[split] < rhs->WorldBound().Center()[split];
				});

			// Chose where to split
			// split using one of the rules (half number of elements)
			int halfIndex = shapes.size() / 2;

			splitDistance = shapes[halfIndex]->WorldBound().pMax[split];

			while (halfIndex < shapes.size() - 1 && shapes[halfIndex + 1]->WorldBound().pMin[split]  < splitDistance)
			{
				halfIndex++;
			}

			if (halfIndex == shapes.size()) halfIndex--;

			std::vector<Shape*>::iterator first = shapes.begin();
			std::vector<Shape*>::iterator half = shapes.begin() + halfIndex + 1;
			std::vector<Shape*>::iterator last = shapes.end();

			std::vector<Shape*> left(first, half);

			halfIndex = shapes.size() / 2;
			while (halfIndex > 0 && shapes[halfIndex - 1]->WorldBound().pMax[split] > splitDistance)
			{
				halfIndex--;
			}
			if (halfIndex == -1) halfIndex++;
			half = shapes.begin() + halfIndex;
			std::vector<Shape*> right(half, last);

			if (left.size() == shapes.size() || right.size() == shapes.size())
			{
				leftChild = nullptr;
				rightChild = nullptr;
				leafShapes = shapes;
			}

			leftChild = new KDNode(left, maxShapesInBox, depth + 1);
			rightChild = new KDNode(right, maxShapesInBox, depth + 1);
		}
	}

	~KDNode()
	{
		if (leftChild != nullptr)
		{
			delete leftChild;
			delete rightChild;
		}
	}

	// find all leaf nodes hit by ray
	bool Hit(const Ray& ray, SurfaceInteraction* isect, float tMin, float tMax)
	{
		// Check if leaf
		if (leftChild == nullptr)
		{
			// Intersect with shapes
			bool hitAnything = false;
			float lastT;
			for (auto shape : leafShapes)
			{
				lastT = ray.tMax;
				if (shape->Intersect(ray, isect))
				{
					return true;
					// Ignore intersection outside AABB
					if (ray.tMax < tMin || ray.tMax > tMax)
						ray.tMax = lastT;
					else
					{
						hitAnything = true;
						lastT = ray.tMax;
					}
				}
			}
			return hitAnything;
		}

		float t = (splitDistance - ray.origin[dimSplit]) / ray.direction[dimSplit];
		KDNode* left = leftChild;
		KDNode* right = rightChild;

		// TODO: check what child is first
		if (ray.direction[dimSplit] > 0)
		{
			std::swap(left, right);
		}

		if (t <= tMin)
		{
			return right->Hit(ray, isect, tMin, tMax);
		}
		else if (t >= tMax)
		{
			return left->Hit(ray, isect, tMin, tMax);
		}
		else
		{
			if (left->Hit(ray, isect, tMin, t))
				return true;
			return right->Hit(ray, isect, t, tMax);
		}
	}

	KDNode* leftChild;
	KDNode* rightChild;

	std::vector<Shape*> leafShapes;

	float splitDistance;
	int dimSplit;
};

class KDTree : public Accelerator
{
public:
	KDTree(std::vector<Shape*> s, int maxShapesInBox = 2)
		: shapes(s)
	{
		// calculate bounds of current AABB
		bounds = shapes[0]->WorldBound();
		for (size_t i = 1; i < s.size(); i++)
			bounds = Union(bounds, shapes[i]->WorldBound());

		root = new KDNode(shapes, maxShapesInBox);
	}
	~KDTree()
	{
		delete root;
		for (auto& shape : shapes)
		{
			shape->~Shape();
		}
	}

	bool Intersect(const Ray& ray, SurfaceInteraction* isect) const;
private:
	std::vector<Shape*> shapes;
	KDNode* root;
	Bounds3 bounds;
};

inline bool KDTree::Intersect(const Ray& ray, SurfaceInteraction* isect) const
{
	bool hitAnything = false;
	float tMin = Infinity;
	float tMax = Infinity;
	// Check if root is hit by ray
	if (bounds.IntersectP(ray, &tMin, &tMax))
	{
		// if ray hits root node
		if (root->Hit(ray, isect, tMin, tMax))
		{
			hitAnything = true;
		}
	}

	return hitAnything;
}