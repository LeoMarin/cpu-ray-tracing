#pragma once

#include <vector>
#include "accelerator.h"
#include "shape.h"

struct BVHNode
{
	BVHNode(size_t startIndex, size_t endIndex, std::vector<Shape*>& shapes, int maxShapesInBox)
		: startIndex(startIndex), endIndex(endIndex), tMin(Infinity)
	{
		// calculate bounds of current AABB
		bounds = shapes[startIndex]->WorldBound();
		for(size_t i = startIndex + 1; i < endIndex; i++)
			bounds = Union(bounds, shapes[i]->WorldBound());

		// check if AABB contains more than permited number of shapes
		if(endIndex - startIndex <= maxShapesInBox)
		{
			// create leaf node
			leftChild = nullptr;
			rightChild = nullptr;
		}
		else
		{
			// find largest axis
			int largestAxis = MaxDimension(bounds.Diagonal());
			// sort on that axis
			std::sort(shapes.begin() + startIndex, shapes.begin() + endIndex, [largestAxis](const Shape* lhs, const Shape* rhs)
				{
					return lhs->WorldBound().Center()[largestAxis] < rhs->WorldBound().Center()[largestAxis];
				});
			// split using one of the rules (half number of elements)
			size_t halfIndex = (endIndex + startIndex) * 0.5f;
			// call BVHNode()
			leftChild = new BVHNode(startIndex, halfIndex, shapes, maxShapesInBox);
			rightChild = new BVHNode(halfIndex, endIndex, shapes, maxShapesInBox);
		}

	}

	~BVHNode()
	{
		if(leftChild != nullptr)
		{
			delete leftChild;
			delete rightChild;
		}
	}

	// find all leaf nodes hit by ray
	bool Hit(const Ray& ray, std::vector<BVHNode*>& hitNodes)
	{
		// if node is hit by ray
		if(bounds.IntersectP(ray, &tMin))
		{
			// is not leaf
			if(leftChild != nullptr)
			{
				// check if ray hits child nodes
				bool hitAnything = false;
				if(leftChild->Hit(ray, hitNodes)) hitAnything = true;
				if(rightChild->Hit(ray, hitNodes)) hitAnything = true;
				return hitAnything;
			}
			// is leaf
			else
			{
				// if ray hits leaf node push to the stack
				hitNodes.push_back(this);
				return true;
			}
		}

		return false;
	}
	Bounds3 bounds;
	BVHNode* leftChild;
	BVHNode* rightChild;
	size_t startIndex; // inclusive
	size_t endIndex; // exclusive
	float tMin;
};

class BVH : public Accelerator
{
public:
	BVH(std::vector<Shape*> s, int maxShapesInBox = 2)
		: shapes(s)
	{
		root = new BVHNode(0, shapes.size(), shapes, maxShapesInBox);
	}
	~BVH()
	{
		delete root;
		for(auto& shape : shapes)
		{
			shape->~Shape();
		}
	}

	bool Intersect(const Ray& ray, SurfaceInteraction* isect) const;
private:
	std::vector<Shape*> shapes;
	BVHNode* root;
};

inline bool BVH::Intersect(const Ray& ray, SurfaceInteraction* isect) const
{
	std::vector<BVHNode*> hitNodes;
	hitNodes.reserve(10);
	bool hitAnything = false;
	// if ray hits root node
	if(root->Hit(ray, hitNodes))
	{
		// loop every leaft node hit
		for(auto& node : hitNodes)
		{
			// if closest hit point of node is closer than current hit 
			if(node->tMin < ray.tMax)
			{
				// loop every object in node
				for(size_t i = node->startIndex; i < node->endIndex; i++)
				{
					if(shapes[i]->Intersect(ray, isect))
					{
						hitAnything = true;
					}
				}
			}
		}
	}

	return hitAnything;
}
