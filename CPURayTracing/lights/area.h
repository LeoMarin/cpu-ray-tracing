#pragma once

#include "material.h"
#include "light.h"
#include "shape.h"

class Area : public Light
{
public:
	Area(Shape* shape, const Vector3f& color, float intensity)
		:shape(shape), color(color), intensity(intensity)
	{
	}

	virtual Vector3f Le(const Ray& shadowRay) const
	{
		return Vector3f();
	}

	virtual Vector3f SampleLi(const Ray& shadowRay, SurfaceInteraction* isect, SurfaceInteraction* shadowIsect) const
	{
		float dot = Dot(shadowRay.direction, isect->normal);
		if(dot < 0)
			return Vector3f();
		// Ir = Is * 1/(4*pi*r2)
		float ir = intensity / Vector3f(shadowIsect->point -shadowRay.origin).LengthSquared();
		return color * dot * ir * isect->material->Albedo();
	}

	virtual Point3f Position() const
	{
		return shape->PointInsideShape();
	}

	virtual Material* LightMaterial() const { return shape->ShapeMaterial(); }

private:
	Shape* shape;
	Vector3f color;
	float intensity;
};
