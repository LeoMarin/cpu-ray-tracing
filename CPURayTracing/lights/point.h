#pragma once

#include "material.h"
#include "light.h"

class PointLight : public Light
{
public:
	PointLight(const Point3f& position, const Vector3f& color, float intensity)
		:position(position), color(color), intensity(intensity)
	{
	}

	virtual Vector3f Le(const Ray& shadowRay) const
	{
		return Vector3f();
	}

	virtual Vector3f SampleLi(const Ray& shadowRay, SurfaceInteraction* isect, SurfaceInteraction* shadowIsect) const
	{
		float dot = Dot(shadowRay.direction, isect->normal);
		if(dot < 0)
			return Vector3f();
		// Ir = Is * 1/(4*pi*r2)
		float ir = intensity / (position - shadowRay.origin).LengthSquared();
		return color * dot * ir * isect->material->Albedo();
	}

	virtual Point3f Position() const
	{
		return position;
	}

private:
	Point3f position;
	Vector3f color;
	float intensity;
};
