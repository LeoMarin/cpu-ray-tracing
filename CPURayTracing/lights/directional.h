#pragma once

#include "light.h"
#include "material.h"
#include "random.h"

class DirectionalLight : public Light
{
public:
	DirectionalLight(const Vector3f& direction, const Vector3f& color, float intensity)
		:direction(direction), color(color), intensity(intensity)
	{
	}

	virtual Vector3f Le(const Ray& shadowRay) const
	{
		return Vector3f();
	}

	virtual Vector3f SampleLi(const Ray& shadowRay, SurfaceInteraction* isect, SurfaceInteraction* shadowIsect) const
	{
		float dot = Dot(shadowRay.direction, isect->normal) * 0.0001;
		if(dot < 0)
			return Vector3f();
		return color * dot * intensity * isect->material->Albedo();
	}

	virtual Point3f Position() const
	{
		return Point3f((direction + RandomPointInUnitSphere()) * (-1000));
	}

private:
	Vector3f direction;
	Vector3f color;
	float intensity;
};
