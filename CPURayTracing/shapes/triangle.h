#pragma once

#include <vector>
#include "shape.h"

struct Vertex
{
	int positionIndex;
	int uvIndex;
	int normalIndex;
};

struct Face
{
	Vertex vertices[3];
};

// Model Definition
class TriangleMesh
{
public:
	// Model Public Methods
	TriangleMesh(Transform ObjectToWorld, const char* fileName, Material* material, std::vector<Shape*>& objects);
	~TriangleMesh() { if(material) delete material; }

	Point3f VertexPosition(int ithFace, int nthVertex) const;
	Vector3f VertexNormal(int ithFace, int nthVertex) const;
	Point3f VertexUV(int ithFace, int nthVertex) const;

	size_t NumberOfVertices() const;
	size_t NumberOfFaces() const;

	// each face looks like: [ {posI, uvI, normI}, {posI, uvI, normI}, {posI, uvI, normI} ]
	std::vector<Face> faces;

	Material* material;
private:
	// Model Private Variables
	std::vector<Point3f> positions;
	std::vector<Vector3f> normals;
	std::vector<Point3f> uvs;
};

class Triangle : public Shape
{
public:
	// MeshTriangle Public Methods
	Triangle(Transform ObjectToWorld, TriangleMesh* mesh, int faceIndex)
		: Shape(ObjectToWorld), mesh(mesh), faceIndex(faceIndex) {}

	Triangle(Transform ObjectToWorld, Transform WorldToObject, TriangleMesh* mesh, int faceIndex)
		: Shape(ObjectToWorld, WorldToObject), mesh(mesh), faceIndex(faceIndex) {}

	~Triangle() {}

	// Return true if ray intersects triangle
	bool Intersect(const Ray& ray, SurfaceInteraction* isect) const override;

	// Returns AABB of triangle in object space
	Bounds3 ObjectBound() const;
	// Returns AABB of triangle in world space
	Bounds3 WorldBound() const;
private:
	// MeshTriangle Private Data
	TriangleMesh* mesh;
	int faceIndex;
};