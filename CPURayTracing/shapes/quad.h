#pragma once

#include "shape.h"
#include "random.h"

class Quad : public Shape
{
public:
	// Plane Public Methods
	Quad(Transform transform, Material* material) : Shape(transform), material(material) {}

	~Quad() { delete material; }
	// Return true if ray intersects plane
	bool Intersect(const Ray& ray, SurfaceInteraction* isect) const;
	// Calculate plane normal in point
	inline Normal3f NormalInPoint(const Point3f& point) const;
	// Returns AABB of plane in object space
	virtual Bounds3 ObjectBound() const;

	// Returns point inside quad
	virtual Point3f PointInsideShape() const;
	// Material
	virtual Material* ShapeMaterial() const { return material; }
private:
	// Plane Private Data
	Material* material;
};

inline bool Quad::Intersect(const Ray& r, SurfaceInteraction* isect) const
{
	// Transform ray to object space
	Ray ray = WorldToObject(r);

	if(std::abs(ray.direction.y) < ShadowEpsilon)
		return false;

	// Intersect y plane
	float t = -ray.origin.y / ray.direction.y;

	if(ray(t).x > 1 ||
		ray(t).x < -1 ||
		ray(t).z > 1 ||
		ray(t).z < -1)
		return false;

	// check hit
	if(t > ShadowEpsilon && r.tMax > t)
	{
		r.tMax = t;
		isect->point = ObjectToWorld(ray(t));
		isect->normal = ObjectToWorld(NormalInPoint(ray(t)));
		isect->material = material;
		return true;
	}

	return false;
}

inline Normal3f Quad::NormalInPoint(const Point3f& point) const
{
	// v = p - center
	// vector v has direction of sphere normal in point p
	return Normal3f(0, 1, 0);
}

inline Bounds3 Quad::ObjectBound() const
{
	return Bounds3(Point3f(-1, -ShadowEpsilon, -1), Point3f(1, ShadowEpsilon, 1));
}

inline Point3f Quad::PointInsideShape() const
{
	return ObjectToWorld(Point3f(RandomFloat() * 2 - 1, 0, RandomFloat() * 2 - 1));
}
