#pragma once

#include "shape.h"

class Plane : public Shape
{
public:
	// Plane Public Methods
	Plane() : material(nullptr) {}
	Plane(Transform transform, Material* material) : Shape(transform), material(material) {}

	~Plane() { delete material; }
	// Return true if ray intersects plane
	bool Intersect(const Ray& ray, SurfaceInteraction* isect) const;
	// Calculate plane normal in point
	inline Normal3f NormalInPoint(const Point3f& point) const;
	// Returns AABB of plane in object space
	virtual Bounds3 ObjectBound() const;
private:
	// Plane Private Data
	Material* material;
};

inline bool Plane::Intersect(const Ray& r, SurfaceInteraction* isect) const
{
	// Transform ray to object space
	Ray ray = WorldToObject(r);

	if (std::abs(ray.direction.y) < ShadowEpsilon)
		return false;

	float t = -ray.origin.y / ray.direction.y;
	// check hit
	if (t > ShadowEpsilon && r.tMax > t)
	{
		r.tMax = t;
		isect->point = ObjectToWorld(ray(t));
		isect->normal = ObjectToWorld(NormalInPoint(ray(t)));
		isect->material = material;
		return true;
	}

	return false;
}

inline Normal3f Plane::NormalInPoint(const Point3f& point) const
{
	// v = p - center
	// vector v has direction of sphere normal in point p
	return Normal3f(0, 1, 0);
}

inline Bounds3 Plane::ObjectBound() const
{
	float constexpr minNum = std::numeric_limits<float>::lowest();
	float constexpr maxNum = std::numeric_limits<float>::max();
	return Bounds3(Point3f(maxNum, ShadowEpsilon, maxNum), Point3f(minNum, -ShadowEpsilon, minNum));
}
