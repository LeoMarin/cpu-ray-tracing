#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include "triangle.h"

TriangleMesh::TriangleMesh(Transform ObjectToWorld, const char* fileName, Material* material, std::vector<Shape*>& objects)
	: positions(), normals(), faces(), material(material)
{
	// open .obj file
	std::ifstream objFile(fileName);
	if (!objFile.is_open())
	{
		std::cout << "OBJ file not opened correctly" << std::endl;
		return;
	}

	// read .obj line by line
	std::string line;
	while (!objFile.eof())
	{
		// read line
		std::getline(objFile, line);
		std::istringstream iss(line.c_str());

		// get prefix
		std::string prefix;
		iss >> prefix;

		// read vertex
		if (prefix == "v")
		{
			Point3f temp;
			iss >> temp.x >> temp.y >> temp.z;
			positions.push_back(temp);
		}
		// read texture coordinates
		else if (prefix == "vt")
		{
			Point3f temp;
			iss >> temp.x >> temp.y;
			uvs.push_back(temp);
		}
		// read normal
		else if (prefix == "vn")
		{
			Vector3f temp;
			iss >> temp.x >> temp.y >> temp.z;
			normals.push_back(temp);
		}
		// read face
		else if (prefix == "f")
		{
			// ever face is described by 3 vertices
			// vertices consist of position, uv and normal
			// each line looks like: *** f pos/uv/norm pos/uv/norm pos/uv/norm ***

			Vertex vertex;
			Face face;
			char trash;

			// read 3 vertices in face
			for (int i = 0; i < 3; i++)
			{
				iss >> vertex.positionIndex;
				iss >> trash;
				iss >> vertex.uvIndex;
				iss >> trash;
				iss >> vertex.normalIndex;

				// in wavefront obj all indices start at 1, not 0
				vertex.positionIndex--;
				vertex.uvIndex--;
				vertex.normalIndex--;

				face.vertices[i] = vertex;
			}

			// store current face in m_Faces
			faces.push_back(face);
		}
	}

	Transform WorldToObject = ObjectToWorld.GetInverseMatrix();
	for(size_t i = 0; i < faces.size(); i++)
	{
		objects.push_back(new Triangle(ObjectToWorld, WorldToObject, this, i));
	}
}


size_t TriangleMesh::NumberOfVertices() const
{
	return positions.size();
}

size_t TriangleMesh::NumberOfFaces() const
{
	return faces.size();
}

inline Point3f TriangleMesh::VertexPosition(int ithFace, int nthVertex) const
{	
	int i = faces[ithFace].vertices[nthVertex].positionIndex;
	return positions[i];
}

inline Vector3f TriangleMesh::VertexNormal(int ithFace, int nthVertex) const
{
	int i = faces[ithFace].vertices[nthVertex].normalIndex;
	return normals[i];
}

inline Point3f TriangleMesh::VertexUV(int ithFace, int nthVertex) const
{
	int i = faces[ithFace].vertices[nthVertex].uvIndex;
	return uvs[i];
}

bool Triangle::Intersect(const Ray& r, SurfaceInteraction* isect) const
{
	// Transform ray to object space
	Ray ray = WorldToObject(r);

	// Moller-Trumbore ray/triangle intersection

	// Rename vertices for easy access
	const Point3f& a = mesh->VertexPosition(faceIndex, 0);
	const Point3f& b = mesh->VertexPosition(faceIndex, 1);
	const Point3f& c = mesh->VertexPosition(faceIndex, 2);

	// Find two edges of a triangle
	Vector3f edge1 = b - a;
	Vector3f edge2 = c - a;

	// calculate determinante
	Vector3f pVec = Cross(ray.direction, edge2);
	float det = Dot(edge1, pVec);

	// if determinante is near zero, the ray does not intersect triangle plane
	if (det > -ShadowEpsilon && det < ShadowEpsilon)
		return false;

	// used to avoid floating point division
	float invDet = 1 / det;

	// distance from vertex to ray origin
	Vector3f tVec = ray.origin - a;

	// u and v barycentric coordinates
	// calculate u parameter and test bounds
	float u = Dot(tVec, pVec) * invDet;
	if (u < 0 || u > 1)
		return false;

	// calculate v parameter and test bounds
	Vector3f qVec = Cross(tVec, edge1);
	float v = Dot(ray.direction, qVec) * invDet;
	if (v < 0 || u + v > 1)
		return false;

	// calculate ray t parameter for intersection
	float t = Dot(edge2, qVec) * invDet;

	// if this is the closest object hit, store relevant values
	if (t < ray.tMax && t > ShadowEpsilon)
	{
		r.tMax = t;
		isect->point = ObjectToWorld(ray(t));
		isect->material = mesh->material;

		if(r.direction.x > 0)
		{
			// calculate normal in current point
			isect->normal = ObjectToWorld(Normal3f(
				(1 - u - v) * mesh->VertexNormal(faceIndex, 0) +
				u * mesh->VertexNormal(faceIndex, 1) +
				v * mesh->VertexNormal(faceIndex, 2)));
		}
		else
		{
			Normal3f normal = Normal3f(UnitVector(Cross(edge1, edge2)));
			if(Dot(normal, ray.direction) > 0)
				isect->normal = ObjectToWorld(-normal);
			else
				isect->normal = ObjectToWorld(normal);
		}


		return true;
	}

	return false;
}

Bounds3 Triangle::WorldBound() const
{
	// Rename vertices for easy access
	const Point3f& a = mesh->VertexPosition(faceIndex, 0);
	const Point3f& b = mesh->VertexPosition(faceIndex, 1);
	const Point3f& c = mesh->VertexPosition(faceIndex, 2);
	return Union(Bounds3(WorldToObject(a), WorldToObject(b)), WorldToObject(c));
}

Bounds3 Triangle::ObjectBound() const
{
	// Rename vertices for easy access
	const Point3f& a = mesh->VertexPosition(faceIndex, 0);
	const Point3f& b = mesh->VertexPosition(faceIndex, 1);
	const Point3f& c = mesh->VertexPosition(faceIndex, 2);
	return Union(Bounds3(a, b), c);
}