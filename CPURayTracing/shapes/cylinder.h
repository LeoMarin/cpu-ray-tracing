#pragma once

#include "shape.h"

class Cylinder : public Shape
{
public:
	// Cylinder Public Methods
	Cylinder() : radius(0), height(0), material(nullptr) {}
	Cylinder(float r, float height, Transform transform, Material* material) : radius(r), height(height), Shape(transform), material(material) {}

	~Cylinder() { delete material; }
	// Return true if ray intersects cylinder
	bool Intersect(const Ray& ray, SurfaceInteraction* isect) const;
	// Calculate cylinder normal in point
	inline Normal3f NormalInPoint(const Point3f& point) const;
	// Returns AABB of cylinder in object space
	virtual Bounds3 ObjectBound() const;
private:
	// Cylinder Private Methods
	void IntersectCaps(const Ray& ray, float& t2) const;
private:
	// Cylinder Private Data
	float radius;
	float height;
	Material* material;
};

inline bool Cylinder::Intersect(const Ray& r, SurfaceInteraction* isect) const
{
	// Transform ray to object space
	Ray ray = WorldToObject(r);

	// calculate a, b and c factors of quadratic equation
	float a = ray.direction.x * ray.direction.x + ray.direction.z * ray.direction.z;
	float b = 2 * ray.origin.x * ray.direction.x + 2 * ray.origin.z * ray.direction.z;
	float c = ray.origin.x * ray.origin.x + ray.origin.z * ray.origin.z - radius * radius;

	// find roots of the quadratic equation
	float t0, t1, t2;
	if (!Quadratic(a, b, c, &t0, &t1))
		return false; // no real solutions means no hit

	// Check if ray hits cylinder caps
	IntersectCaps(ray, t2);

	// Save correct hit
	bool hit = false;
	float t;

	// check first wrapper hit
	if (t0 > ShadowEpsilon && r.tMax > t0 && ray(t0).y > 0 && ray(t0).y < height)
	{
		t = t0;
		hit = true;
	}

	// check cap hit
	if (!hit && t2 > ShadowEpsilon && r.tMax > t2)
	{
		t = t2;
		hit = true;
	}

	// check second warpper hit
	if (!hit && t1 > ShadowEpsilon && r.tMax > t1 && ray(t1).y > 0 && ray(t1).y < height)
	{
		t = t1;
		hit = true;
	}

	if (hit)
	{
		r.tMax = t;
		isect->point = ObjectToWorld(ray(t));
		isect->normal = ObjectToWorld(NormalInPoint(ray(t)));
		isect->material = material;
		return true;
	}

	return false;
}

inline Normal3f Cylinder::NormalInPoint(const Point3f& point) const
{
	// cap normal
	// if hit xz is smaller than radius, hit is on the cap
	// lower cap
	if (point.y < ShadowEpsilon)
		return Normal3f(0, -1, 0);
	// upper cap
	if (point.y > height - ShadowEpsilon)
		return Normal3f(0, 1, 0);
	// warpper normal
	return Normal3f(UnitVector(Vector3f(point.x, 0, point.z)));
}

inline Bounds3 Cylinder::ObjectBound() const
{
	return Bounds3(Point3f(-radius, 0, -radius), Point3f(radius, height, radius));;
}

inline void Cylinder::IntersectCaps(const Ray& ray, float& t2) const
{
	// calculate hit with lower cap plane
	t2 = -ray.origin.y / ray.direction.y;
	Point3f p2 = ray(t2);
	if ((p2.x * p2.x + p2.z * p2.z) >= radius * radius)
		t2 = -1; // disregard hit if not in radius

				 // calculate hit with upper cap plane
	float t3 = -(ray.origin.y - height) / ray.direction.y;
	Point3f p3 = ray(t3);
	if ((p3.x * p3.x + p3.z * p3.z) >= radius * radius)
		t3 = -1; // disregard hit if not in radius

	if (t2 < 0 || t2 < t3)
		t2 = t3; // Save closer hit that is >0
}