#pragma once

#include "shape.h"

class Cone : public Shape
{
public:
	// Cone Public Methods
	Cone() : angle(0), height(0), material(nullptr), cos2(0), sin2(0), radius(0), cosInv(0) {}
	Cone(float angle, float height, Transform transform, Material* material) : angle(Radians(angle)), height(height), Shape(transform), material(material) 
	{
		// calculate radius of the cone
		radius = height * tan(this->angle);

		// calculate cos squared and cos inverse
		cos2 = cos(this->angle);
		cosInv = 1 / cos2;
		cos2 = cos2 * cos2;

		// calculate sin squared
		sin2 = sin(this->angle);
		sin2 = sin2 * sin2;
	}

	~Cone() { delete material; }
	// Return true if ray intersects cone
	bool Intersect(const Ray& ray, SurfaceInteraction* isect) const;
	// Calculate cone normal in point
	inline Normal3f NormalInPoint(const Point3f& point) const;
	// Returns AABB of cone in object space
	virtual Bounds3 ObjectBound() const;
private:
	// Cone Private Methods
	void IntersectCap(const Ray& ray, float& t2) const;
private:
	// Cone Private Data
	float angle; // in radians
	float height;
	Material* material;

	// hellper variables for calculating hit and normal
	float radius;
	float cos2;
	float sin2;
	float cosInv;
};

inline bool Cone::Intersect(const Ray& r, SurfaceInteraction* isect) const
{
	// Transform ray to object space
	Ray ray = WorldToObject(r);

	// calculate a, b and c factors of quadratic equation
	float a = cos2 * (ray.direction.x * ray.direction.x + ray.direction.z * ray.direction.z) - sin2 * ray.direction.y * ray.direction.y;
	float b = 2 * cos2 * (ray.origin.x * ray.direction.x + ray.origin.z * ray.direction.z) - 2 * sin2 * ray.origin.y * ray.direction.y;
	float c = cos2 * (ray.origin.x * ray.origin.x + ray.origin.z * ray.origin.z) - sin2 * ray.origin.y * ray.origin.y;

	// find roots of the quadratic equation
	float t0, t1, t2;
	if (!Quadratic(a, b, c, &t0, &t1))
		return false; // no real solutions means no hit

	IntersectCap(ray, t2);

	// check first hit
	if (t0 > ShadowEpsilon && r.tMax > t0 && ray(t0).y < 0 && ray(t0).y > -height)
	{
		r.tMax = t0;
		isect->point = ObjectToWorld(ray(t0));
		isect->normal = ObjectToWorld(NormalInPoint(ray(t0)));
		isect->material = material;
		return true;
	}

	// check cap hit
	if (t2 > ShadowEpsilon && r.tMax > t2 && ray(t2).y - ShadowEpsilon < -height )
	{
		r.tMax = t2;
		isect->point = ObjectToWorld(ray(t2));
		isect->normal = ObjectToWorld(NormalInPoint(ray(t2)));
		isect->material = material;
		return true;
	}

	// check second hit
	if (t1 > ShadowEpsilon && r.tMax > t1 && ray(t1).y < 0 && ray(t1).y > -height)
	{
		r.tMax = t1;
		isect->point = ObjectToWorld(ray(t1));
		isect->normal = ObjectToWorld(NormalInPoint(ray(t1)));
		isect->material = material;
		return true;
	}

	return false;
}

inline Normal3f Cone::NormalInPoint(const Point3f& point) const
{
	// Point is on cone cap
	if(point.y - ShadowEpsilon < -height)
		return Normal3f(0, -1, 0);

	// Point is not on the cap
	float y = Vector3f(point).Length() * cosInv;
	Vector3f Y = y * Vector3f(0, -1, 0);
	return Normal3f(UnitVector(Vector3f(point) - Y));
}

inline Bounds3 Cone::ObjectBound() const
{
	return Bounds3(Point3f(-radius, -height, -radius), Point3f(radius, 0, radius));
}

inline void Cone::IntersectCap(const Ray& ray, float& t2) const
{
	// calculate hit cap plane
	t2 = -(ray.origin.y + height) / ray.direction.y;
	Point3f p2 = ray(t2);
	if ((p2.x * p2.x + p2.z * p2.z) >= radius * radius)
		t2 = -1; // disregard hit if not in radius
}
