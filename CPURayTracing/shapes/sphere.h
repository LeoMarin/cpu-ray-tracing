#pragma once

#include "shape.h"

// Sphere Declaration
class Sphere : public Shape
{
public:
	// Sphere Public Methods
	Sphere() : radius(0), material(nullptr) {}
	Sphere(float r, Transform transform, Material* material) : radius(r), Shape(transform), material(material) {}

	~Sphere() { delete material; }
	// Return true if ray intersects sphere
	bool Intersect(const Ray& ray, SurfaceInteraction* isect) const;
	// Calculate sphere normal in point
	inline Normal3f NormalInPoint(const Point3f& point) const;
	// Returns AABB of sphere in object space
	virtual Bounds3 ObjectBound() const;
	// Returns point inside sphere
	virtual Point3f PointInsideShape() const;

	// Material
	virtual Material* ShapeMaterial() const { return material; }
private:
	// Sphere Private Data
	float radius;
	Material* material;
};

inline bool Sphere::Intersect(const Ray& r, SurfaceInteraction* isect) const
{
	// Transform ray to object space
	Ray ray = WorldToObject(r);
	// transform origin point to vector
	Vector3f originVector(ray.origin);

	// calculate a, b and c factors of quadratic equation
	float a = ray.direction.LengthSquared();
	float b = 2 * Dot(originVector, ray.direction);
	float c = originVector.LengthSquared() - radius * radius;

	// find roots of the quadratic equation
	float t0, t1;
	if (!Quadratic(a, b, c, &t0, &t1))
		return false; // no real solutions means no hit

					  // check first hit
	if (t0 > ShadowEpsilon && r.tMax > t0)
	{
		r.tMax = t0;
		isect->point = ObjectToWorld(ray(t0));
		isect->normal = ObjectToWorld(NormalInPoint(ray(t0)));
		isect->material = material;
		return true;
	}

	// check second hit
	if (t1 > ShadowEpsilon && r.tMax > t1)
	{
		r.tMax = t1;
		isect->point = ObjectToWorld(ray(t1));
		isect->normal = ObjectToWorld(NormalInPoint(ray(t1)));
		isect->material = material;
		return true;
	}
	return false;
}

inline Normal3f Sphere::NormalInPoint(const Point3f& point) const
{
	// v = p - center
	// vector v has direction of sphere normal in point p
	return Normal3f(Vector3f(point) / radius);
}

inline Bounds3 Sphere::ObjectBound() const
{
	return Bounds3(Point3f(-radius, -radius, -radius),	Point3f(radius, radius, radius));
}

inline Point3f Sphere::PointInsideShape() const
{
	return ObjectToWorld(Point3f(RandomPointInUnitSphere() * radius));
}
